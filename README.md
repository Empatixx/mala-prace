# Calculator

A simple calculator project written in Python. This project provides basic arithmetic operations such as addition, subtraction, multiplication, and division.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Features](#features)
- [Contributing](#contributing)
- [License](#license)

## Installation

1. Clone the repository:

    ```sh
    git clone https://github.com/your-username/calculator.git
    cd calculator
    ```

2. (Optional) Create a virtual environment:

    ```sh
    python -m venv venv
    source venv/bin/activate  # On Windows use `venv\Scripts\activate`
    ```

3. Install the required dependencies:

    ```sh
    pip install -r requirements.txt
    ```

## Usage

You can use the calculator by running the main script and following the on-screen prompts to perform various arithmetic operations.

### Example

1. Run the calculator:

    ```sh
    python calculator.py
    ```

2. Follow the prompts to perform operations. For example:

    ```text
    Welcome to the Calculator!
    Select operation:
    1. Add
    2. Subtract
    3. Multiply
    4. Divide
    Enter choice(1/2/3/4): 1
    Enter first number: 10
    Enter second number: 5
    10 + 5 = 15
    ```

## Features

- Addition
- Subtraction
- Multiplication
- Division

## Contributing

Contributions are welcome! Please follow these steps to contribute:

1. Fork the repository.
2. Create a new branch:

    ```sh
    git checkout -b feature-branch
    ```

3. Make your changes.
4. Commit your changes:

    ```sh
    git commit -m "Add some feature"
    ```

5. Push to the branch:

    ```sh
    git push origin feature-branch
    ```

6. Open a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
