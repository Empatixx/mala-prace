# Calculator Documentation

Welcome to the documentation for the Calculator project. This document provides an overview of how to use the calculator and details about its functionality.

## Overview

The Calculator project is a simple command-line tool written in Python that allows users to perform basic arithmetic operations including addition, subtraction, multiplication, and division.

## Installation

To install the Calculator project, follow these steps:

1. Clone the repository:

    ```sh
    git clone https://github.com/your-username/calculator.git
    cd calculator
    ```

2. Create a virtual environment (optional but recommended):

    ```sh
    python -m venv venv
    source venv/bin/activate  # On Windows use `venv\Scripts\activate`
    ```

3. Install dependencies:

    ```sh
    pip install -r requirements.txt
    ```

## Usage

To use the calculator, run the main script and follow the prompts to perform the desired operations.

### Example

1. Run the calculator:

    ```sh
    python calculator.py
    ```

2. Follow the on-screen prompts to select an operation and input numbers. For example:

    ```text
    Welcome to the Calculator!
    Select operation:
    1. Add
    2. Subtract
    3. Multiply
    4. Divide
    Enter choice(1/2/3/4): 1
    Enter first number: 10
    Enter second number: 5
    10 + 5 = 15
    ```

## Features

- **Addition**: Adds two numbers.
- **Subtraction**: Subtracts the second number from the first.
- **Multiplication**: Multiplies two numbers.
- **Division**: Divides the first number by the second (if the second number is not zero).

## Contributing

Contributions to improve the Calculator project are welcome. Please refer to the [Contributing](#contributing) section in the README for guidelines.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
